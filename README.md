# PROJET 8

**PROJET 8 - DOCUMENTEZ VOTRE SYSTEME DE GESTION DE PIZZERIA**

**CONTEXTE**

Vous avez déjà rencontré ce scenario dans les projets cités dans le paragraphe "Pré-requis". C'est tout à fait normal, le travail que vous allez faire maintenant est complémentaire à celui des projets précédents.
« OC Pizza » est un jeune groupe de pizzeria en plein essor et spécialisé dans les pizzas livrées ou à emporter. Il compte déjà 5 points de vente et prévoit d’en ouvrir au moins 3 de plus d’ici la fin de l’année. Un des responsable du groupe a pris contact avec vous afin de mettre en place un système informatique sur-mesures, déployé dans toutes ses pizzerias et qui lui permettrait notamment :

    • D’être plus efficace dans la gestion des commandes, de leur réception à leur livraison en passant par leur préparation ;
    • De suivre en temps réel les commandes passées et en préparation ;
    • De suivre en temps réel le stock d’ingrédients restants pour savoir quelles pizzas sont encore réalisables ;
    • De proposer un site internet pour que les clients puissent :
        o Passer leurs commandes, en plus de la prise de commande par téléphone ou sur place,
        o Payer en ligne leur commande s’il le souhaite, sinon, ils paieront directement à la livraison
        o Modifier ou annuler leur commande tant que celle-ci n’a pas été préparée
    • De proposer un aide mémoire aux pizzaiolos indiquant la recette de chaque pizza
    • D’informer ou notifier les clients sur l’état de leur commande

Le client a déjà fait une petite prospection et les logiciels existants qu’il a pu trouver ne lui conviennent pas.

Dans votre proposition de solution, vous partirez du principe que vous disposez dans votre société de toutes les ressources
et compétences nécessaires à la réalisation du projet.

**TRAVAIL DEMANDE**

En tant qu’analyste-programmeur, votre rôle est évidemment de développer le système mais également de le documenter. 
Votre entreprise est engagé dans une démarche qualité pour la réalisation de ses produits. Dans ce projet, vous allez 
donc vous attacher à produire la documentation attendue, à savoir :

    • Un dossier de conception fonctionnelle : à l’attention de la maîtrise d’ouvrage (MOA) et de la maîtrise d’œuvre (MOE)
    • Un dossier de conception technique : à l’attention des développeurs, mainteneurs et de l’équipe technique du client 
    • Un dossier d’exploitation : à l’attention de l’équipe technique du client
    • Le procès-verbal de livraison finale

Afin d’harmoniser la documentation entre les projets et limiter les oublis, votre entreprise a établi des trames types pour les documents de projet. 

Vous pouvez modifier l'aspect des documents tout en conservant un style professionnel. Ces documents contiennent un plan type à respecter mais vous pouvez l'adapter au contexte (ajout de titres, suppression des parties inappropriées...)
Votre travail sera validé par votre chef de projet (ce rôle est assuré par le mentor qui vous fera passer la soutenance du projet)

Vous utiliserez UML dans les dossiers de conception.

Afin de ne pas refaire une conception complète de zéro, vous pouvez vous appuyer sur le travail que vous avez fait lors des projets :

    • Analysez les besoins de votre client pour son groupe de pizzerias
    • Concevez la solution technique d’un système de gestion de pizzeria

Vous n’avez pas à développer l’application.

**LIVRABLES ATTENDUS**

    • Le dossier de conception fonctionnelle :
        o Un document au format PDF et suivant la trame donnée
        o Si besoin, le schéma du modèle physique de données peut faire l'objet d'un document PDF annexe (au format A3 par exemple)
    • Le dossier de conception technique :
        o Un document au format PDF et suivant la trame donnée
    • Le dossier d'exploitation :
        o Un document au format PDF et suivant la trame donnée
        o Si besoin, une archive ZIP avec les éléments annexes (fichiers de configuration, drivers JDBC...)
    • Le PV de livraison finale : un document au format PDF et suivant la trame donnée

